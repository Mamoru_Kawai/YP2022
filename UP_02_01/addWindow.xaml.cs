﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UP_02_01
{
    /// <summary>
    /// Логика взаимодействия для addWindow.xaml
    /// </summary>
    public partial class addWindow : Window
    {
        DataBaseContext personalDB;
        public addWindow()
        {
            InitializeComponent();
            personalDB = new DataBaseContext();

        }

        private void addbutton2_Click(object sender, RoutedEventArgs e)
        {
            string name = addname.Text.Trim();
            string surname = addsurname.Text.Trim();
            string patronymic = addpatronymic.Text.Trim();
            string date = addnumber.Text.Trim();
            string number = adddate.Text.Trim();
            string departament = adddepartament.Text.Trim();

            staff staffs = new staff(name, surname, patronymic, number, date, departament);
            personalDB.staffs.Add(staffs);
            personalDB.SaveChanges();
            Close();
            
        }
    }
}
