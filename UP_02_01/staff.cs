﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UP_02_01
{
    public class staff
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string patronymic { get; set; }
        public string date { get; set; }
        public string number { get; set; }
        public string departament { get; set; }


        public staff(string name, string surname, string patronymic, string date, string number, string departament) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.date = date;
        this.number = number;
        this.departament = departament;
        }
    }
}
