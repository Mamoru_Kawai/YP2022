using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UP_02_01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<staff> staff = new List<staff>();
            using (var personalDB = new DataBaseContext())
            {
                var lan = from a in personalDB.staffs select a;
                foreach (var worker in lan)
                {
                    staff.Add(worker);

                }
            }
            Tabl.ItemsSource = staff;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            addWindow addWindow = new addWindow();
            addWindow.Show();
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            List<staff> staff = new List<staff>();
            using (var personalDB = new DataBaseContext())
            {
                var lan = from a in personalDB.staffs select a;
                foreach (var worker in lan)
                {
                    staff.Add(worker);

                }
            }
            Tabl.ItemsSource = staff;
        }

        private void ExcelButton_Click(object sender, RoutedEventArgs e)
        {
            List<staff> staff = new List<staff>();
            using (var personalDB = new DataBaseContext())
            {
                var lan = from a in personalDB.staffs select a;
                foreach (var worker in lan)
                {
                    staff.Add(worker);

                }
            }
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("personal1");
                worksheet.Cells["A1"].Value = "id";
                worksheet.Cells["B1"].Value = "Имя";
                worksheet.Cells["C1"].Value = "Фамилия";
                worksheet.Cells["D1"].Value = "Отчество";
                worksheet.Cells["E1"].Value = "Номер";
                worksheet.Cells["F1"].Value = "Дата рождения";
                worksheet.Cells["G1"].Value = "Отдел";
                worksheet.Cells["A2"].LoadFromCollection(staff);
                string path = "Reports";
                if (Directory.Exists(path) == false)
                {
                    Directory.CreateDirectory(path);
                }
                string file = "персонал.xlsx";
                path = System.IO.Path.Combine(path, file);
                FileInfo fi = new FileInfo(path);
                excelPackage.SaveAs(fi);
            }

        }


        private void JsonButton_Click(object sender, RoutedEventArgs e)
        {
            List<staff> staff = new List<staff>();
            using (var personalDB = new DataBaseContext())
            {
                var lan = from a in personalDB.staffs select a;
                foreach (var worker in lan)
                {
                    staff.Add(worker);
                }
            }
            var expjson = JsonConvert.SerializeObject(staff);
            string path = "Reports";
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
            string file = "персонал.json";
            path = System.IO.Path.Combine(path, file);
            File.WriteAllText(path, expjson.ToString());
        }
        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<staff> staff = new List<staff>();
            using (var personalDB = new DataBaseContext())
            {
                var lan = from a in personalDB.staffs select a;
                foreach (var worker in lan)
                {
                    if (worker.name.Contains(SearchBox.Text) || worker.patronymic.Contains(SearchBox.Text) || worker.date.Contains(SearchBox.Text) || worker.number.Contains(SearchBox.Text) || worker.departament.Contains(SearchBox.Text))
                    {
                        staff.Add(worker);
                    }
                }
            }
            Tabl.ItemsSource = staff;
        }
    }
}
